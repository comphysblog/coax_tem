// Gmsh project created on Tue Aug 14 22:02:37 2018
SetFactory("OpenCASCADE");
//+
Disk(1) = {0, 0, 0, 1.5, 1.5};
//+
Disk(3) = {0, 0, 0, 9, 9};
//+
BooleanDifference{ Surface{3}; Delete; }{ Surface{1}; Delete; }
//+
Physical Surface("vacuum", 1) = {3};
//+
Physical Line("inner", 2) = {1};
//+
Physical Line("outer", 3) = {2};
//+
Transfinite Line {1} = 50 Using Progression 1;
//+
Transfinite Line {2} = 50 Using Progression 1;
